Bash Script Programs
Language: Bash
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
         Gloire Rubambiza (https://github.com/rubambig)
Class: CIS 361- System Programming
Semester / Year: Winter 2017

The scripts in part 1 are combined to create a pipeline which generates
a key word in context. It goes through a file line by line and performs a
circular shift on each line for every combination with the 'shiftLines' script.
Then it filters out any line that starts with a noise word found in 'noise.txt'
using the 'filterNoiseWords' script. Finally, it generates a list of all lines
remaining that is sorted alphabetically with duplicate lines removed. This is
done with the 'createReport' script.

The 'EasyGrader' script in part 2 is used to unzip a data directory (cis361p4.zip)
containing student's projects, creates sub-directories for each student under the
current directory, puts files into their respective directories, and then tests each
student's project. The results of all the test are put into a file called 'report'.

For full specifications see 'Project 3 - Bash Scripts.pdf'.

Usage:
Part 1:
./shiftLines <inputData> | ./filterNoiseWords noise.txt | ./createReport <outputFile>
Part 2:
./EasyGrader
Run 'cleanup' to get rid of files from 'EasyGrader'.